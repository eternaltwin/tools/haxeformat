# Haxeformat

Rust implementation of the [Haxe serialization format](https://haxe.org/manual/std-serialization.html),
with serde support.

[Documentation](https://docs.rs/haxeformat)

# Usage 

```rust
use serde::{ Serialize, Deserialize };

#[derive(Debug, Eq, PartialEq, Serialize, Deserialize)]
struct Person {
    name: String,
    surname: String,
    age: u32,
}

let person = Person { name: "John".to_owned(), surname: "Doe".to_owned(), age: 42 };
let serialized = "oy4:namey4:Johny7:surnamey3:Doey3:agei42g";

assert_eq!(haxeformat::from_str::<Person>(serialized).unwrap(), person);
assert_eq!(haxeformat::to_string::<Person>(&person).unwrap(), serialized);
```
