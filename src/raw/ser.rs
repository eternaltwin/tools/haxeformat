use super::{HaxeDate, ObjectRef, Result, StringRef};

#[derive(Copy, Clone, Debug)]
pub enum StringArg<'a> {
    Str(&'a str),
    Ref(StringRef),
}

impl<'a, T: AsRef<str>> From<&'a T> for StringArg<'a> {
    fn from(string: &'a T) -> Self {
        StringArg::Str(string.as_ref())
    }
}
impl<'a> From<StringRef> for StringArg<'a> {
    fn from(string: StringRef) -> Self {
        StringArg::Ref(string)
    }
}
pub trait HaxeSerialize<'ser, State> {
    fn serialize<S: HaxeSerializer>(&'ser self, state: State, serializer: S) -> Result<S::Ok>;
}

pub trait HaxeSerializer {
    type Ok;
    type String: StringSer<Ok = Self::Ok>;
    type Bytes: ObjectSer<Ok = Self::Ok>;
    type Date: ObjectSer<Ok = Self::Ok>;
    type Array: SeqSer<Ok = Self::Ok>;
    type List: SeqSer<Ok = Self::Ok>;
    type Struct: StringMapSer<Ok = Self::Ok>;
    type StringMap: StringMapSer<Ok = Self::Ok>;
    type IntMap: IntMapSer<Ok = Self::Ok>;
    type ObjectMap: ObjMapSer<Ok = Self::Ok>;
    type Class: StringMapSer<Ok = Self::Ok> + NamedSer;
    type EnumByIndex: SeqSer<Ok = Self::Ok> + NamedSer;
    type EnumByName: SeqSer<Ok = Self::Ok> + NamedVariantSer;
    type ClassDef: StringSer<Ok = Self::Ok>;
    type EnumDef: StringSer<Ok = Self::Ok>;
    type Custom: SeqSer<Ok = Self::Ok> + NamedSer;

    fn serialize_null(self) -> Result<Self::Ok>;
    fn serialize_bool(self, v: bool) -> Result<Self::Ok>;
    fn serialize_int(self, v: i64) -> Result<Self::Ok>;
    fn serialize_float(self, v: f64) -> Result<Self::Ok>;
    fn serialize_object_ref(self, v: ObjectRef) -> Result<Self::Ok>;
    fn serialize_string(self, v: StringArg<'_>) -> Result<Self::String>;
    fn serialize_bytes(self, v: &[u8]) -> Result<Self::Bytes>;
    fn serialize_date(self, v: HaxeDate) -> Result<Self::Date>;
    fn serialize_array(self) -> Result<Self::Array>;
    fn serialize_list(self) -> Result<Self::List>;
    fn serialize_struct(self) -> Result<Self::Struct>;
    fn serialize_string_map(self) -> Result<Self::StringMap>;
    fn serialize_int_map(self) -> Result<Self::IntMap>;
    fn serialize_object_map(self) -> Result<Self::ObjectMap>;
    fn serialize_class(self, name: StringArg<'_>) -> Result<Self::Class>;
    fn serialize_enum_by_index(
        self,
        name: StringArg<'_>,
        variant_index: usize,
        nb_params: usize,
    ) -> Result<Self::EnumByIndex>;
    fn serialize_enum_by_name(
        self,
        name: StringArg<'_>,
        variant_name: StringArg<'_>,
        nb_params: usize,
    ) -> Result<Self::EnumByName>;
    fn serialize_class_def(self, name: StringArg<'_>) -> Result<Self::ClassDef>;
    fn serialize_enum_def(self, name: StringArg<'_>) -> Result<Self::EnumDef>;
    fn serialize_custom(self, name: StringArg<'_>) -> Result<Self::Custom>;
    fn serialize_exception<'ser, S, E: HaxeSerialize<'ser, S> + ?Sized>(
        self,
        state: S,
        exception: &'ser E,
    ) -> Result<Self::Ok>;
}
pub trait StringSer {
    type Ok;
    fn string_ref(&mut self) -> StringRef;
    fn end(self) -> Result<Self::Ok>;
}

pub trait ObjectSer {
    type Ok;
    fn object_ref(&mut self) -> ObjectRef;
    fn end(self) -> Result<Self::Ok>;
}

pub trait SeqSer: ObjectSer {
    fn serialize_elem<'ser, S, E: HaxeSerialize<'ser, S> + ?Sized>(
        &mut self,
        state: S,
        elem: &'ser E,
    ) -> Result<()>;
}

pub trait MapSer: ObjectSer {
    fn serialize_value<'ser, S, V: HaxeSerialize<'ser, S> + ?Sized>(
        &mut self,
        state: S,
        value: &'ser V,
    ) -> Result<()>;
}

pub trait IntMapSer: MapSer {
    fn serialize_key(&mut self, key: i64) -> Result<()>;
}

pub trait StringMapSer: MapSer {
    type MapStringKey: StringSer<Ok = ()>;
    fn serialize_key(&mut self, key: StringArg<'_>) -> Result<Self::MapStringKey>;
}

pub trait ObjMapSer: MapSer {
    fn serialize_key<'ser, S, K: HaxeSerialize<'ser, S> + ?Sized>(
        &mut self,
        state: S,
        key: &'ser K,
    ) -> Result<()>;
}

pub trait NamedSer {
    fn name(&mut self) -> StringRef;
}

pub trait NamedVariantSer: NamedSer {
    fn variant_name(&mut self) -> StringRef;
}
