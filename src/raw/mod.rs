pub mod de;
mod error;
mod reader;
pub mod ser;
mod writer;

pub use de::{HaxeDeserialize, HaxeDeserializer};
pub use error::HaxeError;
pub use reader::ReadDeserializer;
pub use ser::{HaxeSerialize, HaxeSerializer};
pub use writer::WriteSerializer;

pub type Result<T> = std::result::Result<T, HaxeError>;

#[derive(Copy, Clone, Debug, Eq, PartialEq, Hash)]
pub struct ObjectRef(usize);

#[derive(Copy, Clone, Debug, Eq, PartialEq, Hash)]
pub struct StringRef(usize);

// only supports seconds precision
#[derive(Copy, Clone, Debug, Eq, PartialEq, Hash)]
pub struct HaxeDate(i64);

impl HaxeDate {
    /// Minimum supported date: 0000-01-01 00h00m00s
    pub const MIN: Self = HaxeDate(-62167219200);
    /// Maximum supported date: 9999-12-31 23h59m59s
    pub const MAX: Self = HaxeDate(253402300799);

    pub fn try_from_timestamp(timestamp: i64) -> Option<Self> {
        if timestamp < Self::MIN.0 || timestamp > Self::MAX.0 {
            None
        } else {
            Some(Self(timestamp))
        }
    }

    pub fn from_timestamp(timestamp: i64) -> Self {
        Self::try_from_timestamp(timestamp).expect("invalid date: year outside range 0...9999")
    }

    pub fn try_new(date: chrono::NaiveDateTime) -> Option<Self> {
        Self::try_from_timestamp(date.timestamp())
    }

    pub fn new(date: chrono::NaiveDateTime) -> Self {
        Self::from_timestamp(date.timestamp())
    }
}

impl From<HaxeDate> for chrono::NaiveDateTime {
    fn from(date: HaxeDate) -> Self {
        chrono::NaiveDateTime::from_timestamp_opt(date.0, 0).expect("invalid HaxeDate")
    }
}

macro_rules! ref_map_struct {
    ($name:ident, $kind:ty) => {
        pub struct $name<T>(Vec<Option<T>>);

        impl<T> $name<T> {
            pub fn new() -> Self {
                $name(Vec::new())
            }

            pub fn insert(&mut self, key: $kind, value: T) -> Option<T> {
                if key.0 >= self.0.len() {
                    self.0.resize_with(key.0 + 1, || None);
                }
                std::mem::replace(&mut self.0[key.0], Some(value))
            }

            pub fn get(&self, key: $kind) -> Option<&T> {
                self.0.get(key.0).and_then(|v| v.as_ref())
            }

            pub fn get_mut(&mut self, key: $kind) -> Option<&mut T> {
                self.0.get_mut(key.0).and_then(|v| v.as_mut())
            }
        }

        impl<T> Default for $name<T> {
            fn default() -> Self {
                Self::new()
            }
        }
    };
}

ref_map_struct!(ObjectRefMap, ObjectRef);
ref_map_struct!(StringRefMap, StringRef);

#[allow(non_upper_case_globals)]
mod chars {
    pub const DATE_FORMAT: &str = "%Y-%m-%d %H:%M:%S";

    #[inline(always)]
    pub fn base64_encode(val: u8) -> u8 {
        const BASE64_CHARS: &[u8] =
            b"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789%:";
        BASE64_CHARS[val as usize]
    }

    #[inline(always)]
    pub fn base64_decode(byte: u8) -> Option<u8> {
        match byte {
            b'A'..=b'Z' => Some(byte - b'A'),
            b'a'..=b'z' => Some(byte - b'a' + 26),
            b'0'..=b'9' => Some(byte - b'0' + 52),
            b'%' => Some(62),
            b':' => Some(63),
            _ => None,
        }
    }

    pub const URL_ESCAPE: u8 = b'%';

    #[inline(always)]
    pub fn url_encode(c: u8) -> Option<u8> {
        match c {
            b'a'..=b'z' | b'A'..=b'Z' | b'0'..=b'9' | b'-' | b'_' | b'.' | b'~' => Some(c),
            _ => None,
        }
    }

    #[inline(always)]
    pub fn url_decode(byte: u8) -> Option<u8> {
        match byte {
            b'a'..=b'z' | b'A'..=b'Z' | b'0'..=b'9' | b'-' | b'_' | b'.' | b'~' => Some(byte),
            _ => None,
        }
    }

    #[inline(always)]
    pub fn to_hex_digits(byte: u8) -> (u8, u8) {
        const HEX_DIGITS: &[u8] = b"0123456789ABCDEF";
        (
            HEX_DIGITS[(byte >> 4) as usize],
            HEX_DIGITS[(byte & 0xF) as usize],
        )
    }

    #[inline(always)]
    pub fn from_hex_digits(high: u8, low: u8) -> Option<u8> {
        fn hex(c: u8) -> Option<u8> {
            match c {
                b'0'..=b'9' => Some(c - b'0'),
                b'A'..=b'F' => Some(c - b'A' + 0xA),
                _ => None,
            }
        }
        hex(high).and_then(|h| hex(low).map(|l| l + (h << 4)))
    }

    pub const Array: u8 = b'a';
    pub const StringMap: u8 = b'b';
    pub const Class: u8 = b'c';
    pub const FiniteFloat: u8 = b'd';
    pub const False: u8 = b'f';
    pub const ObjEnd: u8 = b'g';
    pub const SeqEnd: u8 = b'h';
    pub const NonZeroInt: u8 = b'i';
    pub const EnumByIndex: u8 = b'j';
    pub const NaN: u8 = b'k';
    pub const List: u8 = b'l';
    pub const NegativeInf: u8 = b'm';
    pub const Null: u8 = b'n';
    pub const Struct: u8 = b'o';
    pub const PositiveInf: u8 = b'p';
    pub const IntMap: u8 = b'q';
    pub const ObjectRef: u8 = b'r';
    pub const Bytes: u8 = b's';
    pub const True: u8 = b't';
    pub const Date: u8 = b'v';
    pub const ArrayNull: u8 = b'u';
    pub const EnumByName: u8 = b'w';
    pub const Exception: u8 = b'x';
    pub const String: u8 = b'y';
    pub const Zero: u8 = b'z';
    pub const ClassDef: u8 = b'A';
    pub const EnumDef: u8 = b'B';
    pub const Custom: u8 = b'C';
    pub const ObjectMap: u8 = b'M';
    pub const StringRef: u8 = b'R';
    pub const Separator: u8 = b':';
}
