use serde::ser::{self, Serialize as SerdeSerialize, Serializer as SerdeSerializer};

use self::sub_serializers::*;
use super::adapter::AdapterMarker;
use crate::raw::ser::*;
use crate::raw::{HaxeDate, HaxeError, Result};
use crate::value::SerializationCache;

pub struct Serializer<'a, S> {
    ser: S,
    ctx: &'a mut SerializerContext,
}

pub struct SerializerContext {
    use_enum_index: bool,
    cache: SerializationCache<'static>,
}

impl Default for SerializerContext {
    fn default() -> Self {
        Self::new()
    }
}

impl<'a, 'ser, T: SerdeSerialize + ?Sized> HaxeSerialize<'ser, &'a mut SerializerContext> for T {
    fn serialize<S: HaxeSerializer>(
        &'ser self,
        state: &'a mut SerializerContext,
        serializer: S,
    ) -> Result<S::Ok> {
        let ser = Serializer {
            ser: serializer,
            ctx: state,
        };
        self.serialize(ser)
    }
}

impl SerializerContext {
    pub fn new() -> Self {
        SerializerContext {
            use_enum_index: false,
            cache: SerializationCache::new(),
        }
    }

    pub fn use_enum_index(mut self, enabled: bool) -> Self {
        self.use_enum_index = enabled;
        self
    }

    pub fn with<S: HaxeSerializer>(&mut self, serializer: S) -> Serializer<'_, S> {
        Serializer {
            ser: serializer,
            ctx: self,
        }
    }

    fn cache_string<F, T: StringSer>(&mut self, string: &str, f: F) -> Result<T>
    where
        F: FnOnce(StringArg) -> Result<T>,
    {
        // TODO: add some caching for string values
        f(StringArg::Str(string))
    }

    fn serialize_string_key<S: StringMapSer>(
        &mut self,
        serializer: &mut S,
        key: &'static str,
    ) -> Result<()> {
        self.cache
            .with_string(key, |key| serializer.serialize_key(key))?
            .end()?;
        Ok(())
    }
}

macro_rules! impl_serializer_base {
    (
        forwards { $($default_method:ident)* };
        err { $($err_method:ident)* } => $err_expr:expr;
    ) => {
        $(impl_serializer_base!(@err $err_method: $err_expr);)*
        $(impl_serializer_base!(@default $default_method);)*

        type Error = HaxeError;

        #[inline]
        fn is_human_readable(&self) -> bool {
            false
        }
    };
    (@default primitives) => {
        #[inline] fn serialize_u8 (self, v: u8 ) -> Result<Self::Ok> { self.serialize_i64(v.into()) }
        #[inline] fn serialize_u16(self, v: u16) -> Result<Self::Ok> { self.serialize_i64(v.into()) }
        #[inline] fn serialize_u32(self, v: u32) -> Result<Self::Ok> { self.serialize_i64(v.into()) }
        #[inline] fn serialize_i8 (self, v: i8 ) -> Result<Self::Ok> { self.serialize_i64(v.into()) }
        #[inline] fn serialize_i16(self, v: i16) -> Result<Self::Ok> { self.serialize_i64(v.into()) }
        #[inline] fn serialize_i32(self, v: i32) -> Result<Self::Ok> { self.serialize_i64(v.into()) }
        fn serialize_u64(self, v: u64) -> Result<Self::Ok> {
            if v >= i64::max_value() as u64 {
                Err(HaxeError::unrepresentable(format!("integer {} is too big!", v)))
            } else {
                self.serialize_i64(v as i64)
            }
        }

        #[inline] fn serialize_f32(self, v: f32) -> Result<Self::Ok> { self.serialize_f64(v.into()) }

        fn serialize_char(self, v: char) -> Result<Self::Ok> {
            let mut buf = [0u8; 4];
            self.serialize_str(v.encode_utf8(&mut buf))
        }
    };
    (@default newtype) => {
        fn serialize_newtype_struct<T: SerdeSerialize + ?Sized>(self, name: &'static str, value: &T) -> Result<Self::Ok> {
            match AdapterMarker::parse(name) {
                Some(marker) => Err(HaxeError::unrepresentable(format!("unexpected marker struct {:?}", marker))),
                None => value.serialize(self),
            }
        }
    };
    (@default option) => {
        #[inline]
        fn serialize_some<T: SerdeSerialize + ?Sized>(self, value: &T) -> Result<Self::Ok> {
            value.serialize(self)
        }
        #[inline]
        fn serialize_none(self) -> Result<Self::Ok> {
            self.ser.serialize_null()
        }
    };
    (@err bool: $expr:expr) => { #[inline]
        fn serialize_bool(self, _: bool) -> Result<Self::Ok> { $expr }
    };
    (@err int: $expr:expr) => { #[inline]
        fn serialize_i64(self, _: i64) -> Result<Self::Ok> { $expr }
    };
    (@err float: $expr:expr) => { #[inline]
        fn serialize_f64(self, _: f64) -> Result<Self::Ok> { $expr }
    };
    (@err str: $expr:expr) => { #[inline]
        fn serialize_str(self, _: &str) -> Result<Self::Ok> { $expr }
    };
    (@err bytes: $expr:expr) => { #[inline]
        fn serialize_bytes(self, _: &[u8]) -> Result<Self::Ok> { $expr }
    };
    (@err option: $expr:expr) => {
        #[inline]
        fn serialize_some<T: SerdeSerialize + ?Sized>(self, _: &T) -> Result<Self::Ok> { $expr }
        #[inline]
        fn serialize_none(self) -> Result<Self::Ok> { $expr }
    };
    (@err unit: $expr:expr) => {
        #[inline]
        fn serialize_unit(self) -> Result<Self::Ok> { $expr }
        #[inline]
        fn serialize_unit_struct(self, _: &'static str) -> Result<Self::Ok> { $expr }
    };
    (@err seq: $expr:expr) => {
        type SerializeSeq = serde::ser::Impossible<Self::Ok, Self::Error>;
        #[inline]
        fn serialize_seq(self, _: Option<usize>) -> Result<Self::SerializeSeq> { $expr }
    };
    (@err map: $expr:expr) => {
        type SerializeMap = serde::ser::Impossible<Self::Ok, Self::Error>;
        #[inline]
        fn serialize_map(self, _: Option<usize>) -> Result<Self::SerializeMap> { $expr }
    };
    (@err tuple: $expr:expr) => {
        type SerializeTuple = serde::ser::Impossible<Self::Ok, Self::Error>;
        type SerializeTupleStruct = serde::ser::Impossible<Self::Ok, Self::Error>;

        #[inline]
        fn serialize_tuple_struct(self, _: &'static str,
            _: usize) -> Result<Self::SerializeTupleStruct> { $expr }
        #[inline]
        fn serialize_tuple(self, _: usize) -> Result<Self::SerializeTuple> { $expr }
    };
    (@err enum: $expr:expr) => {
        type SerializeTupleVariant = serde::ser::Impossible<Self::Ok, Self::Error>;
        type SerializeStructVariant = serde::ser::Impossible<Self::Ok, Self::Error>;

        #[inline]
        fn serialize_unit_variant(self, _: &'static str, _: u32,
            _: &'static str) -> Result<Self::Ok> { $expr }
        #[inline]
        fn serialize_newtype_variant<T: SerdeSerialize + ?Sized>(self, _: &'static str,
                _: u32, _: &'static str, _: &T) -> Result<Self::Ok> { $expr }
        #[inline]
        fn serialize_tuple_variant(self, _: &'static str, _: u32,
            _: &'static str, _: usize) -> Result<Self::SerializeTupleVariant> { $expr }
        #[inline]
        fn serialize_struct_variant(self, _: &'static str, _: u32,
            _: &'static str, _: usize) -> Result<Self::SerializeStructVariant> { $expr }
    };
    (@err struct: $expr:expr) => {
        type SerializeStruct = serde::ser::Impossible<Self::Ok, Self::Error>;
        #[inline]
        fn serialize_struct(self, _: &'static str,
            _: usize) -> Result<Self::SerializeStruct> { $expr }
    };
}

impl<'a, S: HaxeSerializer> SerdeSerializer for Serializer<'a, S> {
    impl_serializer_base!(
        forwards { primitives option };
        err {} => ();
    );

    type Ok = S::Ok;
    type SerializeSeq = Compound<'a, S>;
    type SerializeStruct = Compound<'a, S>;
    type SerializeTuple = Compound<'a, S>;
    type SerializeTupleStruct = Compound<'a, S>;
    type SerializeMap = MapCompound<'a, S>;
    type SerializeTupleVariant = Compound<'a, S>;
    type SerializeStructVariant = Compound<'a, S>;

    #[inline]
    fn serialize_bool(self, v: bool) -> Result<Self::Ok> {
        self.ser.serialize_bool(v)
    }

    #[inline]
    fn serialize_i64(self, v: i64) -> Result<Self::Ok> {
        self.ser.serialize_int(v)
    }

    #[inline]
    fn serialize_f64(self, v: f64) -> Result<Self::Ok> {
        self.ser.serialize_float(v)
    }

    #[inline]
    fn serialize_str(self, v: &str) -> Result<Self::Ok> {
        let Serializer { ctx, ser } = self;
        ctx.cache_string(v, |v| ser.serialize_string(v))?.end()
    }

    #[inline]
    fn serialize_bytes(self, v: &[u8]) -> Result<Self::Ok> {
        self.ser.serialize_bytes(v)?.end()
    }

    #[inline]
    fn serialize_unit(self) -> Result<Self::Ok> {
        self.serialize_none()
    }

    #[inline]
    fn serialize_seq(self, len: Option<usize>) -> Result<Self::SerializeSeq> {
        // Defaults to Array
        ArraySerializer {
            ser: self.ser,
            ctx: self.ctx,
        }
        .serialize_seq(len)
    }

    #[inline]
    fn serialize_map(self, len: Option<usize>) -> Result<Self::SerializeMap> {
        // Defaults to struct, with string keys
        StructSerializer {
            ser: self.ser,
            ctx: self.ctx,
        }
        .serialize_map(len)
    }

    #[inline]
    fn serialize_tuple(self, len: usize) -> Result<Self::SerializeTuple> {
        // Defaults to Array
        ArraySerializer {
            ser: self.ser,
            ctx: self.ctx,
        }
        .serialize_tuple(len)
    }

    #[inline]
    fn serialize_struct(self, name: &'static str, len: usize) -> Result<Self::SerializeStruct> {
        // Defaults to struct
        StructSerializer {
            ser: self.ser,
            ctx: self.ctx,
        }
        .serialize_struct(name, len)
    }

    #[inline]
    fn serialize_unit_struct(self, _name: &'static str) -> Result<Self::Ok> {
        self.serialize_unit()
    }

    #[inline]
    fn serialize_tuple_struct(
        self,
        name: &'static str,
        len: usize,
    ) -> Result<Self::SerializeTupleStruct> {
        // Defaults to Array
        ArraySerializer {
            ser: self.ser,
            ctx: self.ctx,
        }
        .serialize_tuple_struct(name, len)
    }

    #[inline]
    fn serialize_unit_variant(
        self,
        name: &'static str,
        variant_index: u32,
        variant: &'static str,
    ) -> Result<Self::Ok> {
        ser::SerializeTupleVariant::end(self.serialize_tuple_variant(
            name,
            variant_index,
            variant,
            0,
        )?)
    }

    fn serialize_newtype_variant<T: SerdeSerialize + ?Sized>(
        self,
        name: &'static str,
        variant_index: u32,
        variant: &'static str,
        value: &T,
    ) -> Result<Self::Ok> {
        let mut ser = self.serialize_tuple_variant(name, variant_index, variant, 1)?;
        ser::SerializeTupleVariant::serialize_field(&mut ser, value)?;
        ser::SerializeTupleVariant::end(ser)
    }

    fn serialize_tuple_variant(
        self,
        name: &'static str,
        variant_index: u32,
        variant: &'static str,
        len: usize,
    ) -> Result<Self::SerializeTupleVariant> {
        let Serializer { ser, ctx } = self;
        if ctx.use_enum_index {
            ctx.cache
                .with_named(name, |name| {
                    ser.serialize_enum_by_index(name, variant_index as usize, len)
                })
                .map(move |s| Compound::new(ctx, SerializerKind::EnumByIndex(s)))
        } else {
            ctx.cache
                .with_named_variant(name, variant, |name, variant| {
                    ser.serialize_enum_by_name(name, variant, len)
                })
                .map(move |s| Compound::new(ctx, SerializerKind::EnumByName(s)))
        }
    }

    fn serialize_struct_variant(
        self,
        name: &'static str,
        variant_index: u32,
        variant: &'static str,
        len: usize,
    ) -> Result<Self::SerializeStructVariant> {
        self.serialize_tuple_variant(name, variant_index, variant, len)
    }

    fn serialize_newtype_struct<T: SerdeSerialize + ?Sized>(
        self,
        name: &'static str,
        value: &T,
    ) -> Result<Self::Ok> {
        let Serializer { ser, ctx } = self;
        match AdapterMarker::parse(name) {
            None => value.serialize(Serializer { ser, ctx }),
            Some(AdapterMarker::Date) => value.serialize(DateSerializer { ser }),
            Some(AdapterMarker::Array) => value.serialize(ArraySerializer { ser, ctx }),
            Some(AdapterMarker::List) => value.serialize(ListSerializer { ser, ctx }),
            Some(AdapterMarker::StringMap) => value.serialize(StringMapSerializer { ser, ctx }),
            Some(AdapterMarker::IntMap) => value.serialize(IntMapSerializer { ser, ctx }),
            Some(AdapterMarker::ObjectMap) => value.serialize(ObjectMapSerializer { ser, ctx }),
            Some(AdapterMarker::Class) => value.serialize(ClassSerializer { ser, ctx }),
            Some(AdapterMarker::Struct) => value.serialize(StructSerializer { ser, ctx }),
            Some(AdapterMarker::ClassDef) => value.serialize(ClassDefSerializer { ser, ctx }),
            Some(AdapterMarker::EnumDef) => value.serialize(EnumDefSerializer { ser, ctx }),
            Some(AdapterMarker::Custom) => Err(HaxeError::unrepresentable(
                "custom Haxe serialization isn't supported",
            )),
            Some(AdapterMarker::Exception) => ser.serialize_exception(ctx, value),
            Some(AdapterMarker::Unknown) => Err(HaxeError::unrepresentable(format!(
                "unknown marker type: {}",
                &name[1..]
            ))),
        }
    }
}

mod sub_serializers {
    use super::*;

    pub enum SerializerKind<S: HaxeSerializer> {
        Array(S::Array),
        List(S::List),
        Struct(S::Struct),
        Class(S::Class),
        EnumByName(S::EnumByName),
        EnumByIndex(S::EnumByIndex),
        #[allow(dead_code)] // TODO: implement CustomAdapter
        Custom(S::Custom),
    }

    pub struct Compound<'a, S: HaxeSerializer> {
        ctx: &'a mut SerializerContext,
        ser: SerializerKind<S>,
    }

    impl<'a, S: HaxeSerializer> Compound<'a, S> {
        pub(crate) fn new(ctx: &'a mut SerializerContext, ser: SerializerKind<S>) -> Self {
            Compound { ctx, ser }
        }

        fn do_end(self) -> Result<S::Ok> {
            match self.ser {
                SerializerKind::Array(ser) => ser.end(),
                SerializerKind::List(ser) => ser.end(),
                SerializerKind::Struct(ser) => ser.end(),
                SerializerKind::Class(ser) => ser.end(),
                SerializerKind::EnumByName(ser) => ser.end(),
                SerializerKind::EnumByIndex(ser) => ser.end(),
                SerializerKind::Custom(ser) => ser.end(),
            }
        }

        fn do_serialize_value<T: SerdeSerialize + ?Sized>(&mut self, value: &T) -> Result<()> {
            let ctx = &mut *self.ctx;
            match &mut self.ser {
                SerializerKind::Array(ser) => ser.serialize_elem(ctx, value),
                SerializerKind::List(ser) => ser.serialize_elem(ctx, value),
                SerializerKind::Class(ser) => ser.serialize_value(ctx, value),
                SerializerKind::Struct(ser) => ser.serialize_value(ctx, value),
                SerializerKind::EnumByName(ser) => ser.serialize_elem(ctx, value),
                SerializerKind::EnumByIndex(ser) => ser.serialize_elem(ctx, value),
                SerializerKind::Custom(ser) => ser.serialize_elem(ctx, value),
            }
        }

        fn do_serialize_field(&mut self, name: &'static str) -> Result<()> {
            let Compound { ctx, ser } = self;
            match ser {
                SerializerKind::Class(ser) => ctx.serialize_string_key(ser, name),
                SerializerKind::Struct(ser) => ctx.serialize_string_key(ser, name),
                _ => panic!("can't serialize field name here"),
            }
        }
    }

    pub enum MapSerializerKind<S: HaxeSerializer> {
        Struct(S::Struct),
        StringMap(S::StringMap),
        IntMap(S::IntMap),
        ObjectMap(S::ObjectMap),
    }

    pub struct MapCompound<'a, S: HaxeSerializer> {
        ctx: &'a mut SerializerContext,
        ser: MapSerializerKind<S>,
    }

    impl<'a, S: HaxeSerializer> MapCompound<'a, S> {
        pub(crate) fn new(ctx: &'a mut SerializerContext, ser: MapSerializerKind<S>) -> Self {
            MapCompound { ctx, ser }
        }

        fn do_end(self) -> Result<S::Ok> {
            match self.ser {
                MapSerializerKind::Struct(ser) => ser.end(),
                MapSerializerKind::StringMap(ser) => ser.end(),
                MapSerializerKind::IntMap(ser) => ser.end(),
                MapSerializerKind::ObjectMap(ser) => ser.end(),
            }
        }

        fn do_serialize_key<T: SerdeSerialize + ?Sized>(&mut self, key: &T) -> Result<()> {
            let ctx = &mut *self.ctx;
            match &mut self.ser {
                MapSerializerKind::Struct(ser) => key.serialize(StringKeySerializer { ser, ctx }),
                MapSerializerKind::StringMap(ser) => {
                    key.serialize(StringKeySerializer { ser, ctx })
                }
                MapSerializerKind::IntMap(ser) => key.serialize(IntKeySerializer { ser }),
                MapSerializerKind::ObjectMap(ser) => ser.serialize_key(ctx, key),
            }
        }

        fn do_serialize_value<T: SerdeSerialize + ?Sized>(&mut self, value: &T) -> Result<()> {
            let ctx = &mut *self.ctx;
            match &mut self.ser {
                MapSerializerKind::Struct(ser) => ser.serialize_value(ctx, value),
                MapSerializerKind::StringMap(ser) => ser.serialize_value(ctx, value),
                MapSerializerKind::IntMap(ser) => ser.serialize_value(ctx, value),
                MapSerializerKind::ObjectMap(ser) => ser.serialize_value(ctx, value),
            }
        }
    }

    macro_rules! impl_sub_serializers {
        (@impl ($($name:path),+) for $tyname:ident { $body:tt }) => {
            $(impl<'a, S: HaxeSerializer> $name for $tyname<'a, S> {
                type Ok = S::Ok;
                type Error = HaxeError;

                fn end(self) -> Result<Self::Ok> {
                    self.do_end()
                }

                impl_sub_serializers!(@unpack_body $body);
            })*
        };
        (@unpack_body { $($body:tt)* }) => { $($body)* };
        (impl ($($name:path),+) for $tyname:ident { $($body:tt)* }) => {
            impl_sub_serializers!(@impl ($($name),+) for $tyname {{ $($body)* }});
        }
    }

    impl_sub_serializers! {
        impl (ser::SerializeTupleVariant, ser::SerializeTupleStruct) for Compound {
            #[inline]
            fn serialize_field<T: SerdeSerialize + ?Sized>(&mut self, value: &T) -> Result<()> {
                self.do_serialize_value(value)
            }
        }
    }

    impl_sub_serializers! {
        impl (ser::SerializeStruct) for Compound {
            fn serialize_field<T: SerdeSerialize + ?Sized>(&mut self, name: &'static str, value: &T) -> Result<()> {
                self.do_serialize_field(name)?;
                self.do_serialize_value(value)
            }
        }
    }

    impl_sub_serializers! {
        impl (ser::SerializeStructVariant) for Compound {
            fn serialize_field<T: SerdeSerialize + ?Sized>(&mut self, _name: &'static str, value: &T) -> Result<()> {
                self.do_serialize_value(value)
            }
        }
    }

    impl_sub_serializers! {
        impl (ser::SerializeSeq, ser::SerializeTuple) for Compound {
            #[inline]
            fn serialize_element<T: SerdeSerialize + ?Sized>(&mut self, value: &T) -> Result<()> {
                self.do_serialize_value(value)
            }
        }
    }

    impl_sub_serializers! {
        impl (ser::SerializeMap) for MapCompound {
            #[inline]
            fn serialize_key<T: SerdeSerialize + ?Sized>(&mut self, key: &T) -> Result<()> {
                self.do_serialize_key(key)
            }

            #[inline]
            fn serialize_value<T: SerdeSerialize + ?Sized>(&mut self, value: &T) -> Result<()> {
                self.do_serialize_value(value)
            }
        }
    }
}

struct StringKeySerializer<'a, S> {
    ser: &'a mut S,
    ctx: &'a mut SerializerContext,
}

impl<'a, S: StringMapSer> SerdeSerializer for StringKeySerializer<'a, S> {
    impl_serializer_base!(
        forwards { primitives newtype };
        err {
            bool int float bytes unit option seq map tuple struct enum
        } => Err(HaxeError::unrepresentable("expected string key"));

    );
    type Ok = ();

    #[inline]
    fn serialize_str(self, v: &str) -> Result<Self::Ok> {
        let StringKeySerializer { ctx, ser } = self;
        ctx.cache_string(v, |v| ser.serialize_key(v))?.end()
    }
}

struct IntKeySerializer<'a, S> {
    ser: &'a mut S,
}

impl<'a, S: IntMapSer> SerdeSerializer for IntKeySerializer<'a, S> {
    impl_serializer_base!(
        forwards { primitives newtype };
        err {
            bool float str bytes unit option seq map tuple struct enum
        } => Err(HaxeError::unrepresentable("expected integer key"));
    );
    type Ok = ();

    #[inline]
    fn serialize_i64(self, v: i64) -> Result<Self::Ok> {
        self.ser.serialize_key(v)
    }
}

struct DateSerializer<S> {
    ser: S,
}

impl<S: HaxeSerializer> SerdeSerializer for DateSerializer<S> {
    impl_serializer_base!(
        forwards { primitives newtype option };
        err {
            bool float str bytes unit seq map tuple struct enum
        } => Err(HaxeError::unrepresentable("expected timestamp"));
    );
    type Ok = S::Ok;

    fn serialize_i64(self, v: i64) -> Result<Self::Ok> {
        match HaxeDate::try_from_timestamp(v) {
            Some(date) => self.ser.serialize_date(date)?.end(),
            None => Err(HaxeError::unrepresentable("invalid timestamp")),
        }
    }
}

macro_rules! declare_seq_serializer {(
    name=$name:ident,
    serialize_fn=$serialize_fn:ident,
    serializer_kind=$serializer_kind:path,
    err_msg=$errmsg:expr
) => {
    struct $name<'a, S> {
        ser: S,
        ctx: &'a mut SerializerContext,
    }

    impl<'a, S: HaxeSerializer> SerdeSerializer for $name<'a, S> {
        impl_serializer_base!(
            forwards { primitives newtype option };
            err {
                bool int float str bytes unit map struct enum
            } => Err(HaxeError::unrepresentable($errmsg));
        );
        type Ok = S::Ok;
        type SerializeSeq = Compound<'a, S>;
        type SerializeTuple = Compound<'a, S>;
        type SerializeTupleStruct = Compound<'a, S>;

        #[inline]
        fn serialize_seq(self, _len: Option<usize>) -> Result<Self::SerializeSeq> {
            let $name { ser, ctx } = self;
            ser.$serialize_fn()
                .map(move |s| Compound::new(ctx, $serializer_kind(s)))
        }

        #[inline]
        fn serialize_tuple(self, _len: usize) -> Result<Self::SerializeTuple> {
            self.serialize_seq(None)
        }

        #[inline]
        fn serialize_tuple_struct(self, _name: &'static str,
                _len: usize) -> Result<Self::SerializeTupleStruct> {
            self.serialize_seq(None)
        }
    }
}}

declare_seq_serializer!(
    name = ArraySerializer,
    serialize_fn = serialize_array,
    serializer_kind = SerializerKind::Array,
    err_msg = "expected array"
);

declare_seq_serializer!(
    name = ListSerializer,
    serialize_fn = serialize_list,
    serializer_kind = SerializerKind::List,
    err_msg = "expected list"
);

macro_rules! declare_map_serializer {(
    name=$name:ident,
    serialize_fn=$serialize_fn:ident,
    serializer_kind=$serializer_kind:path,
    err_msg=$errmsg:expr
) => {
    struct $name<'a, S> {
        ser: S,
        ctx: &'a mut SerializerContext,
    }

    impl<'a, S: HaxeSerializer> SerdeSerializer for $name<'a, S> {
        impl_serializer_base!(
            forwards { primitives newtype option };
            err {
                bool int float str bytes unit seq tuple struct enum
            } => Err(HaxeError::unrepresentable($errmsg));
        );
        type Ok = S::Ok;
        type SerializeMap = MapCompound<'a, S>;

        #[inline]
        fn serialize_map(self, _len: Option<usize>) -> Result<Self::SerializeMap> {
            let $name { ser, ctx } = self;
            ser.$serialize_fn()
                .map(move |s| MapCompound::new(ctx, $serializer_kind(s)))
        }
    }
}}

declare_map_serializer!(
    name = StringMapSerializer,
    serialize_fn = serialize_string_map,
    serializer_kind = MapSerializerKind::StringMap,
    err_msg = "expected string map"
);

declare_map_serializer!(
    name = IntMapSerializer,
    serialize_fn = serialize_int_map,
    serializer_kind = MapSerializerKind::IntMap,
    err_msg = "expected int map"
);

declare_map_serializer!(
    name = ObjectMapSerializer,
    serialize_fn = serialize_object_map,
    serializer_kind = MapSerializerKind::ObjectMap,
    err_msg = "expected object map"
);

struct StructSerializer<'a, S> {
    ser: S,
    ctx: &'a mut SerializerContext,
}

impl<'a, S: HaxeSerializer> SerdeSerializer for StructSerializer<'a, S> {
    impl_serializer_base!(
        forwards { primitives newtype option };
        err {
            bool int float str bytes seq tuple enum
        } => Err(HaxeError::unrepresentable("expected struct"));
    );
    type Ok = S::Ok;
    type SerializeMap = MapCompound<'a, S>;
    type SerializeStruct = Compound<'a, S>;

    #[inline]
    fn serialize_unit(self) -> Result<Self::Ok> {
        self.ser.serialize_struct()?.end()
    }

    #[inline]
    fn serialize_unit_struct(self, _name: &'static str) -> Result<Self::Ok> {
        self.serialize_unit()
    }

    #[inline]
    fn serialize_map(self, _len: Option<usize>) -> Result<Self::SerializeMap> {
        let StructSerializer { ser, ctx } = self;
        ser.serialize_struct()
            .map(move |s| MapCompound::new(ctx, MapSerializerKind::Struct(s)))
    }

    #[inline]
    fn serialize_struct(self, _name: &'static str, _len: usize) -> Result<Self::SerializeStruct> {
        let StructSerializer { ser, ctx } = self;
        ser.serialize_struct()
            .map(move |s| Compound::new(ctx, SerializerKind::Struct(s)))
    }
}

macro_rules! declare_string_serializer {(
    name=$name:ident,
    serialize_fn=$serialize_fn:ident,
    err_msg=$errmsg:expr
) => {
    struct $name<'a, S> {
        ser: S,
        ctx: &'a mut SerializerContext,
    }

    impl<'a, S: HaxeSerializer> SerdeSerializer for $name<'a, S> {
        impl_serializer_base!(
            forwards { primitives newtype option };
            err {
                bool int float bytes unit seq map tuple struct enum
            } => Err(HaxeError::unrepresentable($errmsg));
        );
        type Ok = S::Ok;

        #[inline]
        fn serialize_str(self, name: &str) -> Result<Self::Ok> {
            let $name { ctx, ser } = self;
            ctx.cache_string(name, |name| ser.$serialize_fn(name))?.end()
        }
    }
}}

declare_string_serializer!(
    name = ClassDefSerializer,
    serialize_fn = serialize_class_def,
    err_msg = "expected class name (string)"
);

declare_string_serializer!(
    name = EnumDefSerializer,
    serialize_fn = serialize_enum_def,
    err_msg = "expected enum name (string)"
);

struct ClassSerializer<'a, S> {
    ctx: &'a mut SerializerContext,
    ser: S,
}

impl<'a, S: HaxeSerializer> SerdeSerializer for ClassSerializer<'a, S> {
    impl_serializer_base!(
        forwards { primitives newtype option };
        err {
            bool int float str bytes seq map tuple enum
        } => Err(HaxeError::unrepresentable("expected struct"));
    );
    type Ok = S::Ok;
    type SerializeStruct = Compound<'a, S>;

    #[inline]
    fn serialize_unit(self) -> Result<Self::Ok> {
        Err(HaxeError::unrepresentable("expected struct"))
    }

    #[inline]
    fn serialize_unit_struct(self, name: &'static str) -> Result<Self::Ok> {
        let ser = self.serialize_struct(name, 0)?;
        ser::SerializeStruct::end(ser)
    }

    #[inline]
    fn serialize_struct(self, name: &'static str, _len: usize) -> Result<Self::SerializeStruct> {
        let ClassSerializer { ctx, ser } = self;
        let ser = ctx
            .cache
            .with_named(name, |name| ser.serialize_class(name))?;
        Ok(Compound::new(ctx, SerializerKind::Class(ser)))
    }
}
