pub mod adapter;
mod de;
mod ser;

#[cfg(test)]
mod tests;

pub use de::{DeserializeSeed, Deserializer, DeserializerContext};
pub use ser::{Serializer, SerializerContext};

use std::fmt::Display;
use std::io::{Cursor, Read, Write};

use serde::de::{Expected, Unexpected};
use serde::{Deserialize, Serialize};

use crate::{HaxeError, Result, StreamDeserializer, StreamSerializer};

impl serde::de::Error for HaxeError {
    fn custom<T: Display>(msg: T) -> Self {
        HaxeError::custom(msg)
    }

    fn invalid_type(unexp: Unexpected, exp: &dyn Expected) -> Self {
        HaxeError::unexpected_input(exp, unexp)
    }

    fn invalid_value(unexp: Unexpected, exp: &dyn Expected) -> Self {
        HaxeError::unexpected_input(exp, unexp)
    }
}

impl serde::ser::Error for HaxeError {
    fn custom<T: Display>(msg: T) -> Self {
        HaxeError::custom(msg)
    }
}

pub fn from_reader_with<'de, R: Read, T: Deserialize<'de>>(
    reader: R,
    ctx: DeserializerContext,
) -> Result<T> {
    let mut de = StreamDeserializer::new(reader, ctx);
    let value = de.deserialize()?;
    de.end()?;
    Ok(value)
}

pub fn from_reader<'de, R: Read, T: Deserialize<'de>>(reader: R) -> Result<T> {
    from_reader_with(reader, DeserializerContext::new())
}

pub fn from_slice_with<'de, T: Deserialize<'de>>(
    bytes: &[u8],
    ctx: DeserializerContext,
) -> Result<T> {
    from_reader_with(Cursor::new(&bytes), ctx)
}

pub fn from_slice<'de, T: Deserialize<'de>>(bytes: &[u8]) -> Result<T> {
    from_reader(Cursor::new(&bytes))
}

pub fn from_str_with<'de, T: Deserialize<'de>>(
    string: &str,
    ctx: DeserializerContext,
) -> Result<T> {
    from_reader_with(Cursor::new(&string), ctx)
}

pub fn from_str<'de, T: Deserialize<'de>>(string: &str) -> Result<T> {
    from_reader(Cursor::new(&string))
}

pub fn to_writer_with<W: Write, T: Serialize>(
    value: &T,
    writer: W,
    ctx: SerializerContext,
) -> Result<()> {
    StreamSerializer::new(writer, ctx).serialize(value)
}

pub fn to_writer<W: Write, T: Serialize>(value: &T, writer: W) -> Result<()> {
    to_writer_with(value, writer, SerializerContext::new())
}

pub fn to_vec_with<T: Serialize>(value: &T, ctx: SerializerContext) -> Result<Vec<u8>> {
    let mut ser = StreamSerializer::new(Cursor::new(Vec::new()), ctx);
    ser.serialize(value)?;
    Ok(ser.into_inner().into_inner())
}

pub fn to_vec<T: Serialize>(value: &T) -> Result<Vec<u8>> {
    to_vec_with(value, SerializerContext::new())
}

pub fn to_string_with<T: Serialize>(value: &T, ctx: SerializerContext) -> Result<String> {
    let vec = to_vec_with(value, ctx)?;
    Ok(String::from_utf8(vec).expect("invalid utf8 data"))
}

pub fn to_string<T: Serialize>(value: &T) -> Result<String> {
    to_string_with(value, SerializerContext::new())
}
