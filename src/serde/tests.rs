use std::collections::BTreeMap;
use std::fmt::Debug;

use serde::{Deserialize, Serialize};

use super::adapter::*;
use crate::HaxeDate;

fn test_serialization_success<T: Serialize>(value: &T, serialized: &[u8]) {
    let out = super::to_vec(value).unwrap();
    if &out[..] != serialized {
        panic!(
            "serialization output is incorrect:\n  output: {}\nexpected: {}",
            String::from_utf8_lossy(&out),
            String::from_utf8_lossy(serialized),
        )
    }
}

fn test_deserialization_success<'de, T>(value: &T, serialized: &[u8])
where
    T: Deserialize<'de> + Debug + PartialEq,
{
    let deserialized: T = super::from_slice(serialized).unwrap();
    if &deserialized != value {
        panic!(
            "deserialization output is incorrect:\n  output: {:?}\nexpected: {:?}",
            deserialized, value,
        );
    }
}

fn test_roundtrip_success<'de, T>(value: &T, serialized: &'de [u8])
where
    T: Serialize + Deserialize<'de> + Debug + PartialEq,
{
    test_serialization_success(value, serialized);
    test_deserialization_success(value, serialized);
}

fn test_serialization_failure<T: Serialize>(value: &T, error: &str) {
    let err = super::to_vec(value).unwrap_err().to_string();
    assert_eq!(&err, error);
}

fn test_deserialization_failure<'de, T: Deserialize<'de> + Debug>(
    serialized: &'de [u8],
    error: &str,
) {
    let deserialized: Result<T, _> = super::from_slice(serialized);
    let err = deserialized.unwrap_err().to_string();
    assert_eq!(&err, error);
}

#[derive(Debug, PartialEq, Serialize, Deserialize)]
struct NewtypeStruct<T>(T);

#[derive(Debug, PartialEq, Serialize, Deserialize)]
struct UnitStruct;

#[derive(Debug, PartialEq, Serialize, Deserialize)]
struct SimpleStruct {
    foo: usize,
    bar: String,
    baz: bool,
    quux: f32,
}

#[test]
fn simple_structs() {
    let val = SimpleStruct {
        foo: 1200,
        bar: "hello".into(),
        baz: false,
        quux: 3.25,
    };

    test_roundtrip_success(&NewtypeStruct(0), b"z");
    test_roundtrip_success(&UnitStruct, b"n");
    test_roundtrip_success(&val, b"oy3:fooi1200y3:bary5:helloy3:bazfy4:quuxd3.25g");
    test_roundtrip_success(
        &ClassAdapter(val),
        b"cy12:SimpleStructy3:fooi1200y3:bary5:helloy3:bazfy4:quuxd3.25g",
    );
}

#[derive(Debug, PartialEq, Serialize, Deserialize)]
enum SimpleEnum {
    Unit,
    Newtype(i32),
    Tuple(bool, bool),
    Struct { c: char, n: u8 },
}

#[test]
fn simple_enum() {
    test_roundtrip_success(&SimpleEnum::Unit, b"wy10:SimpleEnumy4:Unit:0");
    test_roundtrip_success(&SimpleEnum::Newtype(-1), b"wy10:SimpleEnumy7:Newtype:1i-1");
    test_roundtrip_success(
        &SimpleEnum::Tuple(true, false),
        b"wy10:SimpleEnumy5:Tuple:2tf",
    );
    test_roundtrip_success(
        &SimpleEnum::Struct { c: 'A', n: 0 },
        b"wy10:SimpleEnumy6:Struct:2y1:Az",
    );

    test_deserialization_failure::<SimpleEnum>(
        b"wy10:SimpleEnumy7:Newtype:2i0i1",
        "unexpected trailing values in sequence or map",
    );
    test_deserialization_failure::<SimpleEnum>(
        b"wy10:SimpleEnumy4:Unit:1i0",
        "unexpected trailing values in sequence or map",
    );
}

#[test]
fn null_and_options() {
    test_roundtrip_success(&(), b"n");
    test_roundtrip_success::<Option<bool>>(&None, b"n");
    test_roundtrip_success(&Some(42), b"i42");
}

#[test]
fn sequence_adapters() {
    #[derive(Debug, PartialEq, Serialize, Deserialize)]
    struct Foo(i32, i32, i32);

    const ARRAY: &[u8] = b"ai1i2i3h";
    const LIST: &[u8] = b"li1i2i3h";

    test_roundtrip_success(&[1, 2, 3], ARRAY);
    test_roundtrip_success(&(1, 2, 3), ARRAY);
    test_roundtrip_success(&Foo(1, 2, 3), ARRAY);
    test_roundtrip_success(&ListAdapter([1, 2, 3]), LIST);
    test_roundtrip_success(&ListAdapter((1, 2, 3)), LIST);
    test_roundtrip_success(&ListAdapter(Foo(1, 2, 3)), LIST);
    test_serialization_failure(&ArrayAdapter(()), "unrepresentable type: expected array");
    test_serialization_failure(&ArrayAdapter(42), "unrepresentable type: expected array");
    test_serialization_failure(&ListAdapter("hello"), "unrepresentable type: expected list");
    test_deserialization_failure::<[u8; 2]>(
        b"ai1i2i3h",
        "unexpected trailing values in sequence or map",
    );
}

#[test]
fn map_adapters() {
    fn make_map<T: Ord>(a: T, b: T, c: T) -> BTreeMap<T, i32> {
        let mut map = BTreeMap::new();
        map.insert(a, 10);
        map.insert(b, 20);
        map.insert(c, 30);
        map
    }

    let str_map = make_map("A".to_owned(), "B".to_owned(), "C".to_owned());
    test_roundtrip_success(&str_map, b"oy1:Ai10y1:Bi20y1:Ci30g");
    test_roundtrip_success(&StructAdapter(str_map.clone()), b"oy1:Ai10y1:Bi20y1:Ci30g");
    test_roundtrip_success(
        &StringMapAdapter(str_map.clone()),
        b"by1:Ai10y1:Bi20y1:Ci30h",
    );
    test_roundtrip_success(&IntMapAdapter(make_map(1, 2, 3)), b"q:1i10:2i20:3i30h");
    test_roundtrip_success(
        &ObjectMapAdapter(make_map(vec![1], vec![2], vec![3])),
        b"Mai1hi10ai2hi20ai3hi30h",
    );
    test_serialization_failure(
        &IntMapAdapter(str_map),
        "unrepresentable type: expected integer key",
    );
}

#[test]
fn extra_adapters() {
    let date = chrono::NaiveDate::from_ymd_opt(2020, 2, 29)
        .and_then(|d| d.and_hms_opt(12, 34, 56))
        .and_then(HaxeDate::try_new)
        .unwrap();
    test_roundtrip_success(&DateAdapter(date), b"v2020-02-29 12:34:56");
    test_roundtrip_success(&ClassDefAdapter("MyClass".to_owned()), b"Ay7:MyClass");
    test_roundtrip_success(&EnumDefAdapter("MyEnum".to_owned()), b"By6:MyEnum");

    test_roundtrip_success(&DateAdapter(Some(date)), b"v2020-02-29 12:34:56");
    test_roundtrip_success(&ClassDefAdapter(Some("MyClass".to_owned())), b"Ay7:MyClass");
    test_roundtrip_success(&EnumDefAdapter(Some("MyEnum".to_owned())), b"By6:MyEnum");

    test_roundtrip_success(&DateAdapter::<Option<HaxeDate>>(None), b"n");
    test_roundtrip_success(&ClassDefAdapter::<Option<String>>(None), b"n");
    test_roundtrip_success(&EnumDefAdapter::<Option<String>>(None), b"n");
}

#[test]
fn serialize_int_too_big() {
    test_serialization_failure(
        &u64::max_value(),
        "unrepresentable type: integer 18446744073709551615 is too big!",
    );
}
