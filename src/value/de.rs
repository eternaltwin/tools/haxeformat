use indexmap::IndexMap;

use super::{HaxeObject, HaxeValue};
use crate::raw::de::{self, StringWithRef};
use crate::raw::{
    HaxeDeserialize, HaxeDeserializer, HaxeError, ObjectRef, StringRef, StringRefMap,
};

pub struct DeserializationCache {
    str_cache: StringRefMap<String>,
}

impl Default for DeserializationCache {
    fn default() -> Self {
        Self::new()
    }
}

impl DeserializationCache {
    pub fn new() -> Self {
        Self {
            str_cache: StringRefMap::new(),
        }
    }

    pub fn put_string(&mut self, string: String, str_ref: StringRef) {
        self.str_cache.insert(str_ref, string);
    }

    pub fn get_string(&self, str_ref: StringRef) -> Result<&str, HaxeError> {
        self.str_cache.get(str_ref).map(|s| &s[..]).ok_or_else(|| {
            HaxeError::invalid_input(format!("invalid string reference: {:?}", str_ref))
        })
    }
}

impl<'a, 'de> HaxeDeserialize<'de, &'a mut DeserializationCache> for HaxeValue {
    fn deserialize<D: HaxeDeserializer<'de>>(
        state: &'a mut DeserializationCache,
        deserializer: D,
    ) -> Result<Self, HaxeError> {
        let visitor = ValueVisitor { state };
        deserializer.deserialize_any(visitor)
    }
}

struct ValueVisitor<'a> {
    state: &'a mut DeserializationCache,
}

impl<'a> ValueVisitor<'a> {
    fn get_cloned_string(&mut self, (string, str_ref): StringWithRef) -> Result<String, HaxeError> {
        if let Some(s) = string {
            self.state.put_string(s, str_ref);
        }
        self.state.get_string(str_ref).map(|s| s.to_owned())
    }
}

impl<'a> ValueVisitor<'a> {
    fn deserialize_string_map<'de, A: de::StrMapAccess<'de>>(
        mut self,
        mut map: A,
    ) -> Result<IndexMap<String, HaxeValue>, HaxeError> {
        let mut contents = IndexMap::new();
        while let Some(key) = map.next_key()? {
            let val = map.next_value(&mut *self.state)?;
            contents.insert(self.get_cloned_string(key)?, val);
        }
        Ok(contents)
    }

    fn deserialize_seq<'de, A: de::SeqAccess<'de>>(
        self,
        mut seq: A,
    ) -> Result<Vec<HaxeValue>, HaxeError> {
        let mut contents = seq
            .size_hint()
            .map(Vec::with_capacity)
            .unwrap_or_else(Vec::new);
        while let Some(elem) = seq.next_element(&mut *self.state)? {
            contents.push(elem);
        }
        Ok(contents)
    }
}

impl<'a, 'de> de::HaxeVisitor<'de> for ValueVisitor<'a> {
    type Value = HaxeValue;

    fn visit_null(self) -> Result<Self::Value, HaxeError> {
        Ok(HaxeValue::Null)
    }

    fn visit_bool(self, v: bool) -> Result<Self::Value, HaxeError> {
        Ok(HaxeValue::Bool(v))
    }

    fn visit_int(self, v: i64) -> Result<Self::Value, HaxeError> {
        Ok(HaxeValue::Int(v))
    }

    fn visit_float(self, v: f64) -> Result<Self::Value, HaxeError> {
        Ok(HaxeValue::Float(v))
    }

    fn visit_object_ref(self, _v: ObjectRef) -> Result<Self::Value, HaxeError> {
        // TODO: add support
        Err(HaxeError::custom("object references aren't supported"))
    }

    fn visit_str(mut self, v: StringWithRef) -> Result<Self::Value, HaxeError> {
        self.get_cloned_string(v).map(HaxeValue::String)
    }

    fn visit_bytes(self, v: Vec<u8>, _obj: ObjectRef) -> Result<Self::Value, HaxeError> {
        Ok(HaxeObject::Bytes(v).into())
    }

    fn visit_date(self, v: super::HaxeDate, _obj: ObjectRef) -> Result<Self::Value, HaxeError> {
        Ok(HaxeObject::Date(v).into())
    }

    fn visit_array<A: de::SeqAccess<'de>>(
        self,
        seq: A,
        _obj: ObjectRef,
    ) -> Result<Self::Value, HaxeError> {
        let vec = self.deserialize_seq(seq)?;
        Ok(HaxeObject::Array(vec).into())
    }

    fn visit_list<A: de::SeqAccess<'de>>(
        self,
        seq: A,
        _obj: ObjectRef,
    ) -> Result<Self::Value, HaxeError> {
        let vec = self.deserialize_seq(seq)?;
        Ok(HaxeObject::List(vec).into())
    }

    fn visit_struct<A: de::StrMapAccess<'de>>(
        self,
        fields: A,
        _obj: ObjectRef,
    ) -> Result<Self::Value, HaxeError> {
        let fields = self.deserialize_string_map(fields)?;
        Ok(HaxeObject::Struct(fields).into())
    }

    fn visit_string_map<A: de::StrMapAccess<'de>>(
        self,
        map: A,
        _obj: ObjectRef,
    ) -> Result<Self::Value, HaxeError> {
        let contents = self.deserialize_string_map(map)?;
        Ok(HaxeObject::StringMap(contents).into())
    }

    fn visit_int_map<A: de::IntMapAccess<'de>>(
        self,
        mut map: A,
        _obj: ObjectRef,
    ) -> Result<Self::Value, HaxeError> {
        let mut buf = IndexMap::new();
        while let Some(key) = map.next_key()? {
            let val = map.next_value(&mut *self.state)?;
            buf.insert(key, val);
        }
        Ok(HaxeObject::IntMap(buf).into())
    }

    fn visit_object_map<A: de::ObjMapAccess<'de>>(
        self,
        mut map: A,
        _obj: ObjectRef,
    ) -> Result<Self::Value, HaxeError> {
        let mut buf = IndexMap::new();
        while let Some(key) = map.next_key(&mut *self.state)? {
            let val = map.next_value(&mut *self.state)?;
            buf.insert(key, val);
        }
        Ok(HaxeObject::ObjectMap(buf).into())
    }

    fn visit_class<A: de::StrMapAccess<'de>>(
        mut self,
        name: StringWithRef,
        data: A,
        _obj: ObjectRef,
    ) -> Result<Self::Value, HaxeError> {
        let name = self.get_cloned_string(name)?;
        let fields = self.deserialize_string_map(data)?;
        Ok(HaxeObject::Class { name, fields }.into())
    }

    fn visit_enum_by_index<A: de::SeqAccess<'de>>(
        mut self,
        name: StringWithRef,
        variant_index: usize,
        data: A,
        _obj: ObjectRef,
    ) -> Result<Self::Value, HaxeError> {
        let name = self.get_cloned_string(name)?;
        let params = self.deserialize_seq(data)?;
        Ok(HaxeObject::EnumByIndex {
            name,
            tag: variant_index,
            params,
        }
        .into())
    }

    fn visit_enum_by_name<A: de::SeqAccess<'de>>(
        mut self,
        name: StringWithRef,
        variant_name: StringWithRef,
        data: A,
        _obj: ObjectRef,
    ) -> Result<Self::Value, HaxeError> {
        let name = self.get_cloned_string(name)?;
        let tag = self.get_cloned_string(variant_name)?;
        let params = self.deserialize_seq(data)?;
        Ok(HaxeObject::EnumByName { name, tag, params }.into())
    }

    fn visit_class_def(
        mut self,
        name: StringWithRef,
        _obj: ObjectRef,
    ) -> Result<Self::Value, HaxeError> {
        Ok(HaxeValue::ClassDef(self.get_cloned_string(name)?))
    }

    fn visit_enum_def(
        mut self,
        name: StringWithRef,
        _obj: ObjectRef,
    ) -> Result<Self::Value, HaxeError> {
        Ok(HaxeValue::EnumDef(self.get_cloned_string(name)?))
    }

    fn visit_custom<A: de::SeqAccess<'de>>(
        mut self,
        name: StringWithRef,
        data: A,
        _obj: ObjectRef,
    ) -> Result<Self::Value, HaxeError> {
        let name = self.get_cloned_string(name)?;
        let contents = self.deserialize_seq(data)?;
        Ok(HaxeObject::Custom { name, contents }.into())
    }

    fn visit_exception<D: HaxeDeserializer<'de>>(
        self,
        exception: D,
    ) -> Result<Self::Value, HaxeError> {
        let ex = HaxeValue::deserialize(self.state, exception)?;
        Ok(HaxeObject::Exception(ex).into())
    }
}
