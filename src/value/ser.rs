use std::collections::hash_map::{Entry, HashMap};

use super::{HaxeObject, HaxeValue};
use crate::raw::{ser, HaxeError, HaxeSerialize, HaxeSerializer, StringRef};

pub struct SerializationCache<'a> {
    // TODO: use FNV hashing?
    str_cache: HashMap<&'a str, StringRef>,
}

impl<'a> Default for SerializationCache<'a> {
    fn default() -> Self {
        Self::new()
    }
}

impl<'a> SerializationCache<'a> {
    pub fn new() -> Self {
        SerializationCache {
            str_cache: HashMap::new(),
        }
    }

    fn with_raw_str<F, T, E, M>(&mut self, val: &'a str, f: F, map: M) -> Result<T, E>
    where
        F: FnOnce(ser::StringArg<'_>) -> Result<T, E>,
        M: FnOnce(&mut T) -> StringRef,
    {
        match self.str_cache.entry(val) {
            Entry::Occupied(e) => f(ser::StringArg::Ref(*e.get())),
            Entry::Vacant(e) => {
                let mut result = f(ser::StringArg::Str(e.key()))?;
                e.insert(map(&mut result));
                Ok(result)
            }
        }
    }

    pub(crate) fn with_string<F, T: ser::StringSer, E>(
        &mut self,
        val: &'a str,
        f: F,
    ) -> Result<T, E>
    where
        F: FnOnce(ser::StringArg<'_>) -> Result<T, E>,
    {
        self.with_raw_str(val, f, ser::StringSer::string_ref)
    }

    pub(crate) fn with_named<F, T: ser::NamedSer, E>(&mut self, name: &'a str, f: F) -> Result<T, E>
    where
        F: FnOnce(ser::StringArg<'_>) -> Result<T, E>,
    {
        self.with_raw_str(name, f, ser::NamedSer::name)
    }

    pub(crate) fn with_named_variant<F, T: ser::NamedVariantSer, E>(
        &mut self,
        name: &'a str,
        variant_name: &'a str,
        f: F,
    ) -> Result<T, E>
    where
        F: FnOnce(ser::StringArg<'_>, ser::StringArg<'_>) -> Result<T, E>,
    {
        let variant = match self.str_cache.get(variant_name) {
            Some(r) => ser::StringArg::Ref(*r),
            None => ser::StringArg::Str(variant_name),
        };
        let mut result = self.with_named(name, |name| f(name, variant))?;
        if let ser::StringArg::Str(_) = variant {
            self.str_cache.insert(variant_name, result.variant_name());
        }
        Ok(result)
    }
}

impl<'a, 'ser> HaxeSerialize<'ser, &'a mut SerializationCache<'ser>> for HaxeValue {
    fn serialize<S: HaxeSerializer>(
        &'ser self,
        state: &'a mut SerializationCache<'ser>,
        serializer: S,
    ) -> Result<S::Ok, HaxeError> {
        use crate::raw::ser::*;
        match self {
            HaxeValue::Null => serializer.serialize_null(),
            HaxeValue::Int(v) => serializer.serialize_int(*v),
            HaxeValue::Float(v) => serializer.serialize_float(*v),
            HaxeValue::Bool(v) => serializer.serialize_bool(*v),
            HaxeValue::String(v) => state
                .with_string(v, |string| serializer.serialize_string(string))?
                .end(),
            HaxeValue::ClassDef(v) => state
                .with_string(v, |string| serializer.serialize_class_def(string))?
                .end(),
            HaxeValue::EnumDef(v) => state
                .with_string(v, |string| serializer.serialize_enum_def(string))?
                .end(),
            HaxeValue::Object(object) => match object.as_ref() {
                HaxeObject::Exception(v) => serializer.serialize_exception(state, v),
                HaxeObject::Bytes(v) => serializer.serialize_bytes(v)?.end(),
                HaxeObject::Date(v) => serializer.serialize_date(*v)?.end(),
                HaxeObject::Array(list) => {
                    let mut ser = serializer.serialize_array()?;
                    for e in list {
                        ser.serialize_elem(&mut *state, e)?;
                    }
                    ser.end()
                }
                HaxeObject::List(list) => {
                    let mut ser = serializer.serialize_list()?;
                    for e in list {
                        ser.serialize_elem(&mut *state, e)?;
                    }
                    ser.end()
                }
                HaxeObject::StringMap(map) => {
                    let mut ser = serializer.serialize_string_map()?;
                    for (k, v) in map {
                        state.with_string(k, |k| ser.serialize_key(k))?.end()?;
                        ser.serialize_value(&mut *state, v)?;
                    }
                    ser.end()
                }
                HaxeObject::IntMap(map) => {
                    let mut ser = serializer.serialize_int_map()?;
                    for (k, v) in map {
                        ser.serialize_key(*k)?;
                        ser.serialize_value(&mut *state, v)?
                    }
                    ser.end()
                }
                HaxeObject::ObjectMap(map) => {
                    let mut ser = serializer.serialize_object_map()?;
                    for (k, v) in map {
                        ser.serialize_key(&mut *state, k)?;
                        ser.serialize_value(&mut *state, v)?;
                    }
                    ser.end()
                }
                HaxeObject::Struct(fields) => {
                    let mut ser = serializer.serialize_struct()?;
                    for (k, v) in fields {
                        state.with_string(k, |k| ser.serialize_key(k))?.end()?;
                        ser.serialize_value(&mut *state, v)?;
                    }
                    ser.end()
                }
                HaxeObject::Class { name, fields } => {
                    let mut ser =
                        state.with_named(name, |name| serializer.serialize_class(name))?;
                    for (k, v) in fields {
                        state.with_string(k, |k| ser.serialize_key(k))?.end()?;
                        ser.serialize_value(&mut *state, v)?;
                    }
                    ser.end()
                }
                HaxeObject::Custom { name, contents } => {
                    let mut ser =
                        state.with_named(name, |name| serializer.serialize_custom(name))?;
                    for e in contents {
                        ser.serialize_elem(&mut *state, e)?;
                    }
                    ser.end()
                }
                HaxeObject::EnumByName { name, tag, params } => {
                    let mut ser = state.with_named_variant(name, tag, |name, tag| {
                        serializer.serialize_enum_by_name(name, tag, params.len())
                    })?;
                    for e in params {
                        ser.serialize_elem(&mut *state, e)?;
                    }
                    ser.end()
                }
                HaxeObject::EnumByIndex { name, tag, params } => {
                    let mut ser = state.with_named(name, |name| {
                        serializer.serialize_enum_by_index(name, *tag, params.len())
                    })?;
                    for e in params {
                        ser.serialize_elem(&mut *state, e)?;
                    }
                    ser.end()
                }
            },
        }
    }
}
