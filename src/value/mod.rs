use std::hash::{Hash, Hasher};

use indexmap::IndexMap;

pub use crate::raw::HaxeDate;

mod de;
mod ser;

#[cfg(test)]
mod tests;

pub use de::DeserializationCache;
pub use ser::SerializationCache;
#[derive(Clone, Debug)]
pub enum HaxeValue {
    Null,
    // TODO: should we merge ints and floats???
    Int(i64),
    Float(f64),
    Bool(bool),
    String(String),
    ClassDef(String),
    EnumDef(String),
    // TODO: Parametrize this over references types (&'a T, Box, Rc, etc)
    Object(Box<HaxeObject>),
}

impl PartialEq<HaxeValue> for HaxeValue {
    fn eq(&self, other: &Self) -> bool {
        use HaxeValue::*;
        match (self, other) {
            (Null, Null) => true,
            (Int(a), Int(b)) => a == b,
            (Float(a), Float(b)) => normalized_float_bits(*a) == normalized_float_bits(*b),
            (Bool(a), Bool(b)) => a == b,
            (String(a), String(b)) => a == b,
            (ClassDef(a), ClassDef(b)) => a == b,
            (EnumDef(a), EnumDef(b)) => a == b,
            (Object(a), Object(b)) => std::ptr::eq::<HaxeObject>(a.as_ref(), b.as_ref()),
            _ => false,
        }
    }
}

impl Hash for HaxeValue {
    fn hash<H: Hasher>(&self, h: &mut H) {
        use HaxeValue::*;
        std::mem::discriminant(self).hash(h);
        match self {
            Null => (),
            Int(v) => v.hash(h),
            Float(v) => normalized_float_bits(*v).hash(h),
            Bool(v) => v.hash(h),
            String(v) | ClassDef(v) | EnumDef(v) => v.hash(h),
            Object(v) => (v.as_ref() as *const HaxeObject).hash(h),
        }
    }
}

impl Eq for HaxeValue {}

impl From<HaxeObject> for HaxeValue {
    fn from(obj: HaxeObject) -> Self {
        HaxeValue::Object(obj.into())
    }
}

fn normalized_float_bits(f: f64) -> u64 {
    let normalized_float = if f == 0.0 {
        0.0
    } else if f.is_nan() {
        f64::NAN
    } else {
        f
    };
    normalized_float.to_bits()
}

// Uses IndexMap for deterministic iteration
// TODO: use a faster hash, like FNV?
#[derive(Debug, Clone)]
pub enum HaxeObject {
    Exception(HaxeValue),
    Bytes(Vec<u8>),
    Date(HaxeDate),
    Array(Vec<HaxeValue>),
    List(Vec<HaxeValue>),
    StringMap(IndexMap<String, HaxeValue>),
    IntMap(IndexMap<i64, HaxeValue>),
    ObjectMap(IndexMap<HaxeValue, HaxeValue>),
    Struct(IndexMap<String, HaxeValue>),
    Class {
        name: String,
        fields: IndexMap<String, HaxeValue>,
    },
    Custom {
        name: String,
        contents: Vec<HaxeValue>,
    },
    EnumByName {
        name: String,
        tag: String,
        params: Vec<HaxeValue>,
    },
    EnumByIndex {
        name: String,
        tag: usize,
        params: Vec<HaxeValue>,
    },
}
