//! Rust implementation of the [Haxe serialization format](https://haxe.org/manual/std-serialization.html),
//! with serde support.
//!
//! # Usage
//!
//! ```rust
//! use serde::{ Serialize, Deserialize };
//!
//! #[derive(Debug, Eq, PartialEq, Serialize, Deserialize)]
//! struct Person {
//!     name: String,
//!     surname: String,
//!     age: u32,
//! }
//!
//! let person = Person { name: "John".to_owned(), surname: "Doe".to_owned(), age: 42 };
//! let serialized = "oy4:namey4:Johny7:surnamey3:Doey3:agei42g";
//!
//! assert_eq!(haxeformat::from_str::<Person>(serialized).unwrap(), person);
//! assert_eq!(haxeformat::to_string::<Person>(&person).unwrap(), serialized);
//! ```

pub mod raw;
pub mod serde;
pub mod value;

pub use crate::serde::*;
pub use raw::{HaxeDate, HaxeError, Result};

use std::io::{Read, Write};

pub struct StreamSerializer<W, C> {
    ser: raw::WriteSerializer<W>,
    ctx: C,
}

impl<W: Write, C> StreamSerializer<W, C> {
    pub fn new(writer: W, ctx: C) -> Self {
        let ser = raw::WriteSerializer::new(writer);
        Self { ser, ctx }
    }

    pub fn serialize<'a, 'ser, T>(&'a mut self, value: &'ser T) -> Result<()>
    where
        T: raw::HaxeSerialize<'ser, &'a mut C>,
    {
        value.serialize(&mut self.ctx, &mut self.ser)
    }

    pub fn serialize_with<'a, 'ser, T, S, F>(&'a mut self, value: &'ser T, state: F) -> Result<()>
    where
        T: raw::HaxeSerialize<'ser, S>,
        F: FnOnce(&'a mut C) -> S,
    {
        value.serialize(state(&mut self.ctx), &mut self.ser)
    }

    pub fn into_inner(self) -> W {
        self.ser.into_inner()
    }
}

pub struct StreamDeserializer<R, C> {
    de: raw::ReadDeserializer<R>,
    ctx: C,
}

impl<R: Read, C> StreamDeserializer<R, C> {
    pub fn new(reader: R, ctx: C) -> Self {
        let de = raw::ReadDeserializer::new(reader);
        Self { de, ctx }
    }

    pub fn deserialize<'a, 'de, T>(&'a mut self) -> Result<T>
    where
        T: raw::HaxeDeserialize<'de, &'a mut C>,
    {
        self.deserialize_with(|ctx| ctx)
    }

    pub fn deserialize_with<'a, 'de, T, S, F>(&'a mut self, state: F) -> Result<T>
    where
        T: raw::HaxeDeserialize<'de, S>,
        F: FnOnce(&'a mut C) -> S,
    {
        T::deserialize(state(&mut self.ctx), &mut self.de)
    }

    pub fn has_next(&mut self) -> bool {
        self.de.has_next()
    }

    pub fn end(mut self) -> Result<C> {
        self.de.end()?;
        Ok(self.ctx)
    }
}
